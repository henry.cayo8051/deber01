<br>
<div class="container-fluid" style="background-color: black; color: white;">
  <div class="row">
    <div class="col-md-12">
      <p>&copy; 2023 Mi Sitio Web. Todos los derechos reservados.</p>
    </div>
    <div class="col-md-12">
      <ul class="list-inline text-right">
        <li>¡Al cuidado de tu salud visual desde 1990!</li>
        <li>info@hiperopticas.ec</li>
        <li>099 363 2664</li>
      </ul>
    </div>
  </div>
</div>

</body>
</html>