<div class="row">
    <div class="col-md-8">
    <h1 class="center">LISTADO DE CLIENTES NUEVOS</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url ('clientes/nuevo')?>" 
        class="btn btn-primary"> <i class="glyphicon glyphicon-plus" > Agregar Clientes</i></a>

    </div>
</div>
<br>
<?php if ($clientes): ?>
    <table class="table table-striped
    table-bordered table-hover" >
        <thead>
            <tr>
            <th>ID</th>
                <th>CEDULA</th>
                <th>PNOMBRE </th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>EMAIL</th>
                <th>ACCIONES</th>

            </tr>
        </thead>
        <tbody>
        <?php foreach ($clientes
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_cli; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->cedula_cli; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->nombre_cli ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->apellido_cli; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_cli; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_cli; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->email_cli; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" title="Editar Cliente"style="color:green;">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>"title="Eliminar Cliente" onclick="return confirm('ESTA SEGURO DE ELIMINAR ESTE CLIENTE')" style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
              </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else: ?>
    <h1>NO HAY DATOS DE LOS CLIENTES</h1>
    <?php endif ?>
