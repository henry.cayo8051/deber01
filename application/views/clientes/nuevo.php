<h1>Nuevo CLIENTE</h1>
<form class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cedula cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la cedula"
          class="form-control"
          name="cedula_cli" value="" id="cedula_cli">
      </div>
      <div class="col-md-4">
          <label for="">nombre cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_cli" value="" id="nombre_cli">
      </div>
      <div class="col-md-4">
        <label for="">apellido cliente:</label>
        <br>
        <input type="text"
        placeholder="ingrese el apellido"
        class="form-control"
        name="apellido_cli" value="" id="apellido_cli">
      </div>
      <div class="col-md-4">
        <label for="">telefono cliente:</label>
        <br>
        <input type="text"
        placeholder="ingrese el telefono"
        class="form-control"
        name="telefono_cli" value="" id="telefono_cli">
      </div>
      <div class="col-md-4">
        <label for="">direccion cliente:</label>
        <br>
        <input type="text"
        placeholder="ingrese la direcion"
        class="form-control"
        name="direccion_cli" value="" id="direccion_cli">
      </div>
    </div>
    <br>
    
    <br>
    <div class="row">
      <div class="col-md-12">
          <label for=""> email del cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el email"
          class="form-control"
          name="email_cli" value="" id="email_cli">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>