<h1>Nuevo Producto </h1>
<form class="" action="<?php echo site_url(); ?>/productos/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">tipo producto:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tipo"
          class="form-control"
          name="tipo_prod" value="" id="tipo_prod">
      </div>
      <div class="col-md-4">
          <label for="">nombre producto:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_prod" value="" id="nombre_prod">
      </div>
      <div class="col-md-4">
        <label for="">stock del producto:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el stock"
        class="form-control"
        name="stock_prod" value="" id="stock_prod">
      </div>
    </div>
    <br>
    
    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Descripcion del producto:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la descripcion"
          class="form-control"
          name="descripcion_prod" value="" id="descripcion_prod">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>