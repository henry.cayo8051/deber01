<div class="row">
    <div class="col-md-8">
    <h1 class="center">LISTADO DE PRODUCTOS NUEVOS</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url ('productos/nuevo')?>" 
        class="btn btn-primary"> <i class="glyphicon glyphicon-plus" > Agregar Productos</i></a>

    </div>
</div><br>
<?php if ($productos): ?>
    <table class="table table-striped
    table-bordered table-hover" >
        <thead>
            <tr>
            <th>ID</th>
                <th>TIPO</th>
                <th>PNOMBRE DEL PRODUCTO</th>
                <th>STOCK DL PRODUCTO</th>
                <th>DESCRIPCION DEL PRODUCTO</th>
                <th>ACCIONES</th>

            </tr>
        </thead>
        <tbody>
        <?php foreach ($productos
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_prod; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->tipo_prod; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->nombre_prod; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->stock_prod; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->descripcion_prod; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" title="Editar Producto"style="color:green;">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_prod; ?>"title="Eliminar Producto" onclick="return confirm('ESTA SEGURO DE ELIMINAR ESTE PRODUCTO')" style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
              </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else: ?>
    <h1>NO HAY DATOS DE LOS PRODUTOS</h1>
    <?php endif ?>
