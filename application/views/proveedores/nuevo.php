<h1>NUEVO PROVEEDOR</h1>
<form class="" action="<?php echo site_url(); ?>/proveedores/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cedula proveedor:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la cedula"
          class="form-control"
          name="cedula_pro" value="" id="cedula_pro">
      </div>
      <div class="col-md-4">
          <label for="">nombre proveedor:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_pro" value="" id="nombre_pro">
      </div>
      <div class="col-md-4">
        <label for="">apellido proveedor:</label>
        <br>
        <input type="text"
        placeholder="ingrese el apellido"
        class="form-control"
        name="apellido_pro" value="" id="apellido_pro">
      </div>
      <div class="col-md-4">
        <label for="">telefono proveedor:</label>
        <br>
        <input type="text"
        placeholder="ingrese el telefono"
        class="form-control"
        name="telefono_pro" value="" id="telefono_pro">
      </div>
    </div>
    <br>
    
    <br>
    <div class="row">
      <div class="col-md-12">
          <label for=""> direccion del proveedor:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_pro" value="" id="direccion_pro">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/index" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>