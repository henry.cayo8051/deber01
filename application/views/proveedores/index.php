<div class="row">
    <div class="col-md-8">
    <h1 class="center">LISTADO DE PROVEEDORES NUEVOS</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url ('proveedores/nuevo')?>" 
        class="btn btn-primary"> <i class="glyphicon glyphicon-plus" > Agregar proveedor</i></a>

    </div>
</div>
<br>
<?php if ($proveedores): ?>
    <table class="table table-striped
    table-bordered table-hover" >
        <thead>
            <tr>
            <th>ID</th>
                <th>CEDULA</th>
                <th>NOMBRE </th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>

            </tr>
        </thead>
        <tbody>
        <?php foreach ($proveedores
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_pro; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->cedula_pro; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->nombre_pro ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->apellido_pro; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_pro; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_pro; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" title="Editar Proveedor"style="color:green;">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <a href="<?php echo site_url(); ?>/proveedores/eliminar/<?php echo $filaTemporal->id_pro; ?>"title="Eliminar Proveedor" onclick="return confirm('ESTA SEGURO DE ELIMINAR ESTE PROVEEDOR')" style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
              </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else: ?>
    <h1>NO HAY DATOS DE LOS PROVEEDORES</h1>
    <?php endif ?>
