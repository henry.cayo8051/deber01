<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="imagen 1" style="width: 100%; height: 500px;">
    </div>

    <div class="item">
	<img src="<?php echo base_url(); ?>/assets/images/2.jpg" alt="imagen 1" style="width: 100%; height: 500px;">
    </div>

    <div class="item">
	<img src="<?php echo base_url(); ?>/assets/images/3.jpg" alt="imagen 1" style="width: 100%; height: 500px;">   
 </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="panel panel-default text-center">
  <div class="panel-heading">MISION</div>
  <div class="panel-body">
  Satisfacer las necesidades técnicas de los clientes y lograr que su negocio funcione es,
   evidentemente, uno de los principales objetivos de Alabaz. Sin embargo,
    nuestra labor va mucho más allá. Nos gusta estar cerca del cliente, 
	intercambiar opiniones, ideas, hablar sobre su negocio, ayudarle en 
	cualquier cuestión que necesite. No podemos olvidar que nosotros también somos una empresa y, 
	probablemente, hayamos pasado o estemos pasando por las mismas circunstancias que viven 
	nuestros clientes.Por todo ello, la mayor satisfacción para nosotros es verlos crecer, 
	ayudándoles en todo lo que podamos, mucho más allá de simplemente prestar un servicio 
	técnico en PrestaShop.
  </div>
</div>

<div class="panel panel-default text-center">
  <div class="panel-heading">
    <h3 class="panel-title">VISION</h3>
  </div>
  <div class="panel-body">
  En AlabazWeb nos esforzamos por seguir siendo una de las mejores empresas del mundo 
  en el desarrollo de ecommerce PrestaShop para ofrecer siempre a nuestros clientes las 
  mejores soluciones para sus empresas. No basta con crear una tienda online,
   nosotros queremos mejorarla, hacerla eficiente, lograr un diseño adecuado y 
   personal y conseguir que todas las personas que confían en nosotros obtengan 
   el éxito por el que trabajan incansablemente.
  </div>
</div>