<?php
    class Proveedores extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
       $this->load->model('Proveedor');
      }
//Funcion que renderiza la vista index
      public function index(){
       $data['proveedores']=$this->Proveedor->obtenerTodos();
       //print_r($productos);
       $this->load->view('header');
        $this->load->view('proveedores/index', $data);
        $this->load->view('footer');
      }
      public function nuevo(){
        $this->load->view('header');
        $this->load->view('proveedores/nuevo');
        $this->load->view('footer');
      }
      public function guardar(){
        $datosNuevoProveedor=array(
          "cedula_pro"=>$this->input->post('cedula_pro'),
          "nombre_pro"=>$this->input->post('nombre_pro'),
          "apellido_pro"=>$this->input->post('apellido_pro'),
          "telefono_pro"=>$this->input->post('telefono_pro'),  
          "direccion_pro"=>$this->input->post('direccion_pro'),  
        );
        

        //imprime los datos del array que creamos
        print_r($datosNuevoProveedor);
        if ($this->Proveedor->insertar($datosNuevoProveedor)){
          redirect('proveedores/index');
        }else {
          echo "<h1>  Error al insertar datos</h1>";
        }
      }
      // funcion para eliminara istructores 
      public function eliminar($id_pro){
        //echo $id_pro; 
        if ($this->Proveedor->borrar($id_pro)) {//invocando al modelo
        redirect('proveedores/index');
        }else{
        echo "ERROR AL ELIMINAR :(";
        }
    }
    }//Ciere de la clase
  ?>