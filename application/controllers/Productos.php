<?php
    class Productos extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
       $this->load->model('Producto');
      }
//Funcion que renderiza la vista index
      public function index(){
       $data['productos']=$this->Producto->obtenerTodos();
       //print_r($productos);
       $this->load->view('header');
        $this->load->view('productos/index', $data);
        $this->load->view('footer');
      }
      public function nuevo(){
        $this->load->view('header');
        $this->load->view('productos/nuevo');
        $this->load->view('footer');
      }
      public function guardar(){
        $datosNuevoProducto=array(
          "tipo_prod"=>$this->input->post('tipo_prod'),
          "nombre_prod"=>$this->input->post('nombre_prod'),
          "stock_prod"=>$this->input->post('stock_prod'),
          "descripcion_prod"=>$this->input->post('descripcion_prod'),  
        );
        

        //imprime los datos del array que creamos
        print_r($datosNuevoProducto);
        if ($this->Producto->insertar($datosNuevoProducto)){
          redirect('productos/index');
        }else {
         echo "<h1>  Error al insertar datos</h1>";
        }
      }
      // funcion para eliminara istructores 
      public function eliminar($id_prod){
        //echo $id_prod;
        if ($this->Producto->borrar($id_prod)) {//invocando al modelo
        redirect('productos/index');
        }else{
        echo "ERROR AL ELIMINAR :(";
        }
    }
    }//Ciere de la clase
  ?>