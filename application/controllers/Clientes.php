<?php
    class Clientes extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
       $this->load->model('Cliente');
      }
//Funcion que renderiza la vista index
      public function index(){
       $data['clientes']=$this->Cliente->obtenerTodos();
       //print_r($productos);
       $this->load->view('header');
        $this->load->view('clientes/index', $data);
        $this->load->view('footer');
      }
      public function nuevo(){
        $this->load->view('header');
        $this->load->view('clientes/nuevo');
        $this->load->view('footer');
      }
      public function guardar(){
        $datosNuevoCliente=array(
          "cedula_cli"=>$this->input->post('cedula_cli'),
          "nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),
          "telefono_cli"=>$this->input->post('telefono_cli'),  
          "direccion_cli"=>$this->input->post('direccion_cli'),
          "email_cli"=>$this->input->post('email_cli'),  
  

        );
        

        //imprime los datos del array que creamos
        //print_r($datosNuevoCliente);
        if ($this->Cliente->insertar($datosNuevoCliente)){
          redirect('clientes/index');
        }else {
         echo "<h1>  Error al insertar datos</h1>";
        }
      }
      // funcion para eliminara istructores 
      public function eliminar($id_cli){
        //echo $id_cli;
        if ($this->Cliente->borrar($id_cli)) {//invocando al modelo
        redirect('clientes/index');
        }else{
        echo "ERROR AL ELIMINAR :(";
        }
    }
    }//Ciere de la clase
  ?>