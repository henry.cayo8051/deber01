<?php
   class Cliente extends CI_Model
   {

    function __construct()
    {
      parent::__construct();
     
    }
    function insertar ($datos){
        return $this->db->insert("cliente",
        $datos);
    }
    
    function obtenerTodos(){
        $listadoClientes=
        $this->db->get("cliente");
        if($listadoClientes->num_rows()>0){
            return $listadoClientes->result();
        }else {
            return false;
        }
    }
    public function borrar ($id_cli){
        //borrar cliente
            $this->db->where("id_cli", $id_cli);
            if ($this->db->delete('cliente')) {
            return true;
            }else{
            return false;
            }
        }
    }//Cierre de la clase
?>
