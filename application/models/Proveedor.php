<?php
   class Proveedor extends CI_Model
   {

    function __construct()
    {
      parent::__construct();
     
    }
    function insertar ($datos){
        return $this->db->insert("proveedor",
        $datos);
    }
    
    function obtenerTodos(){
        $listadoProveedores=
        $this->db->get("proveedor");
        if($listadoProveedores->num_rows()>0){
            return $listadoProveedores->result();
        }else {
            return false;
        }
    }
    public function borrar ($id_pro){
        //borrar proveedor
          $this->db->where("id_pro", $id_pro);
          if ($this->db->delete('proveedor')) {
            return true;
          }else{
            return false;
          }
      }
    }//Cierre de la clase
  ?>