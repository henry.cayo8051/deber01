<?php
   class Producto extends CI_Model
   {

    function __construct()
    {
      parent::__construct();
     
    }
    function insertar ($datos){
        return $this->db->insert("producto",
        $datos);
    }
    
    function obtenerTodos(){
        $listadoProductos=
        $this->db->get("producto");
        if($listadoProductos->num_rows()>0){
            return $listadoProductos->result();
        }else {
            return false;
        }
    }
    public function borrar ($id_prod){
        //borrar producto
          $this->db->where("id_prod", $id_prod);
          if ($this->db->delete('producto')) {
            return true;
          }else{
            return false;
          }
      }
    }//Cierre de la clase
  ?>